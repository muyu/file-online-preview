package cn.keking.bex5.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@PropertySource("application-bex5.properties")
@ConfigurationProperties("bex5")
@Configuration
public class Bex5Config {
    private String checkSessionUrl;
    private String key;
    private String domain;

    public void setCheckSessionUrl(String checkSessionUrl){
        this.checkSessionUrl =checkSessionUrl;
    }

    public String getCheckSessionUrl(){
        return this.checkSessionUrl;
    }

    public void setkey(String key){
        this.key =key;
    }

    public String getKey(){
        return this.key;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

}
