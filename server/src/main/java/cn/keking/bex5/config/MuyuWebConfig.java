package cn.keking.bex5.config;

import cn.keking.bex5.web.filter.Bex5AuthFilter;
import cn.keking.bex5.web.filter.ForbiddenFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class MuyuWebConfig implements WebMvcConfigurer {
    @Resource
    private Bex5Config bex5Config;

    @Bean
    public FilterRegistrationBean<Bex5AuthFilter> getBex5AuthFilter() {
        Bex5AuthFilter filter = new Bex5AuthFilter(bex5Config.getCheckSessionUrl(), bex5Config.getKey(), bex5Config.getDomain());
        FilterRegistrationBean<Bex5AuthFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(filter);
        //OnlinePreviewController:/onlinePreview,/picturesPreview,/picturesPreview
        registrationBean.addUrlPatterns("/onlinePreview,/picturesPreview,/picturesPreview".split(","));
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<ForbiddenFilter> getForbiddenFilter() {
        ForbiddenFilter filter = new ForbiddenFilter();
        FilterRegistrationBean<ForbiddenFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(filter);
        //IndexController:/,/index,/record,/sponsor,/integrated
        registrationBean.addUrlPatterns("/,/index,/record,/sponsor,/integrated".split(","));
        //FileController:/fileUpload,/deleteFile,/listFiles,/directory
        registrationBean.addUrlPatterns("/fileUpload,/deleteFile,/listFiles,/directory".split(","));
        //OnlinePreviewController:/getCorsFile,/addTask
        registrationBean.addUrlPatterns("/getCorsFile,/addTask".split(","));
        return registrationBean;
    }

    public static void main(String[] args) {
        String[] array ="/,/index,/record,/sponsor,/integrated".split(",");
        for(String url :array){
            System.out.println(url);
        }
    }
}
