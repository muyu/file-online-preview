package cn.keking.bex5.web.filter;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForbiddenFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //forbidden 403
        ((HttpServletResponse)response).setStatus(HttpStatus.FORBIDDEN.value());

        //chain.doFilter(request, response);
    }
}
