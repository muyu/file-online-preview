package cn.keking.bex5.web.filter;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.keking.ServerMain;
import cn.keking.bex5.okhttp.OkHttps;
import cn.keking.service.OfficePluginManager;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Bex5AuthFilter implements Filter {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private String bex5CheckSessionUrl;
    private String key;
    private String domain;


    public Bex5AuthFilter(String bex5CheckSessionUrl,String key,String domain){
        this.bex5CheckSessionUrl =bex5CheckSessionUrl;
        this.key =key;
        this.domain =domain;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
       HttpServletRequest httpServletRequest =(HttpServletRequest)request;

        //通过session鉴权
        String token =request.getParameter("token");
        if(token !=null && token.length() > 0 && checkToken(token)){
            filterChain.doFilter(request, response);
            return;
        }

        //通过session鉴权
        if (checkSession(httpServletRequest)) {
            filterChain.doFilter(request, response);
            return;
        }

        response.setContentType("text/html;charset=utf-8");
        response.getWriter().println("未鉴权");
    }


    /**
     * 鉴权token
     * token=base64(uuid.time.md5(uuid+time+key))
     * @param
     * @return
     */
    public boolean checkToken(String token){
        try{
            if(key.equals(token)){
                return true;
            }
            String[] array = token.split("\\.");
            if(array.length != 3){
                return false;
            }
            String uuid =array[0];
            String time =array[1];
            String md5 =array[2];

            logger.debug("token-->{} key-->{}", uuid,key);

            return SecureUtil.md5(uuid+time+key).toLowerCase().equals(md5);
        } catch (Exception e){
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    public boolean checkSession(HttpServletRequest request) {
        String bessionid = null;

        if(request.getCookies() == null){
            return false;
        }
        for (Cookie cookie : request.getCookies()) {
            if ("bsessionid".equals(cookie.getName())) {
                bessionid = cookie.getValue();
                break;
            }
        }
        if (bessionid == null) {
            return false;
        }

        final String finalBessionid = bessionid;
        OkHttpClient okHttpClient = OkHttps.newHttpClientBuilder().cookieJar(new CookieJar() {
            @Override
            public void saveFromResponse(HttpUrl url, List<okhttp3.Cookie> cookies) {

            }

            @Override
            public List<okhttp3.Cookie> loadForRequest(HttpUrl url) {
                List<okhttp3.Cookie> list =new ArrayList<okhttp3.Cookie>();
                list.add(new okhttp3.Cookie.Builder()
                                .domain(domain)
                                .name("bessionid").value(finalBessionid)
                                .build()
                );
                list.add(new okhttp3.Cookie.Builder()
                                .domain(domain)
                                .name("JSESSIONID").value(finalBessionid)
                                .build()
                );
                return list;
            }
        }).build();
        Request okHttpRequest = new Request.Builder().url(bex5CheckSessionUrl)
                .addHeader("Accept", "application/json")
                .get().build();
        try{
            Response response = okHttpClient.newCall(okHttpRequest).execute();
            JSONObject  json= JSONUtil.parseObj(response.body().string());
            if(json.getBool("flag")){
                return true;
            }
        } catch(Exception e){
            logger.error(e.getMessage(), e);
        }

        return false;
    }

    public static void main(String[] args) throws Exception {
        OkHttpClient okHttpClient = OkHttps.newHttpClientBuilder().cookieJar(new CookieJar() {
            @Override
            public void saveFromResponse(HttpUrl url, List<okhttp3.Cookie> cookies) {

            }

            @Override
            public List<okhttp3.Cookie> loadForRequest(HttpUrl url) {
                List<okhttp3.Cookie> list =new ArrayList<okhttp3.Cookie>();
                list.add(new okhttp3.Cookie.Builder()
                                //.domain("om.sei.com.cn")
                        .name("bessionid").value("336ea49a739e4ce9866f8fe7303e04de.tomcat1")
                        .build()
                );
                list.add(new okhttp3.Cookie.Builder()
                                //.domain("om.sei.com.cn")
                        .name("JSESSIONID").value("336ea49a739e4ce9866f8fe7303e04de.tomcat1")
                        .build()
                );
                return list;
            }
        }).build();

        Request okHttpRequest = new Request.Builder().url("https://om.sei.com.cn/BusinessServer/check-session")
                .addHeader("Accept", "application/json")
                .get().build();
        Response response = okHttpClient.newCall(okHttpRequest).execute();
        System.out.println(response.body().string());
    }

}
