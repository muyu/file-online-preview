package cn.keking.bex5.okhttp;

import okhttp3.OkHttpClient;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class OkHttps {
    public static X509TrustManager TRUST_ALL_CERT_MANAGER = new TrustAllCertManager();

    public static SSLSocketFactory newSSLSocketFactory() {
        return newSSLSocketFactory("TLS");
    }

    /**
     *
     * @param protocol
     * @return
     */
    public static SSLSocketFactory newSSLSocketFactory(String protocol) {
        try {
            SSLContext sc = SSLContext.getInstance(protocol);
            sc.init(null, new TrustManager[] { TRUST_ALL_CERT_MANAGER }, new SecureRandom());
            return sc.getSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }


    public static OkHttpClient.Builder newHttpClientBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.sslSocketFactory(OkHttps.newSSLSocketFactory(), TRUST_ALL_CERT_MANAGER);
        return builder;
    }

    /**
     * 创建一个新的HttpClient
     * @return
     */
    public static OkHttpClient newHttpClient() {
        return newHttpClientBuilder().build();
    }


}
